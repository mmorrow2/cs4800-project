#ifndef SALESITEM_HPP
#define SALESITEM_HPP

#include <QObject>
#include <string>

class SalesItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(double price READ price WRITE setPrice NOTIFY priceChanged)
    Q_PROPERTY(int amount READ amount)

public:
    SalesItem(QString name,  int amount, double price);

    QString name() const;
    void setName(QString name) {m_name = name;}
    double price() const;
    void setPrice(double price) {m_price = price;}
    int amount() const; // Units of item purchased

signals:
    void nameChanged();
    void priceChanged();

private:
    QString m_name;
    double m_price;
    int m_amount;
};

#endif // SALESITEM_HPP
