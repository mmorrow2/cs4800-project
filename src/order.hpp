#ifndef ORDER_HPP
#define ORDER_HPP

#include <QObject>
#include <QList>

#include <salesitem.hpp>


class Order : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString customerName READ customerName)
    Q_PROPERTY(QString customerEmail READ customerEmail)
    Q_PROPERTY(QString customerPhone READ customerPhone)
    Q_PROPERTY(QList<SalesItem*> orderItems READ orderItems)
    Q_PROPERTY(QString orderSummary READ orderSummary)
    Q_PROPERTY(double totalPrice READ totalPrice)


public:
    Order(QString customerName, QString customerEmail, QString customerPhone, QList<SalesItem*> orderItems);
    QString customerName() const;
    QString customerEmail() const;
    QString customerPhone() const;
    QList<SalesItem*> orderItems() const;
    QString orderSummary() const;
    double totalPrice() const;

private:
    QString m_customerName;
    QString m_customerEmail;
    QString m_customerPhone;
    QList<SalesItem*> m_orderItems;
};

#endif // ORDER_HPP
