#include "order.hpp"

Order::Order(QString customerName, QString customerEmail, QString customerPhone, QList<SalesItem*> orderItems) {
    m_customerName = customerName;
    m_customerEmail = customerEmail;
    m_customerPhone = customerPhone;
    m_orderItems = orderItems;
}

QString Order::customerName() const {
    return m_customerName;
}

QString Order::customerEmail() const {
    return m_customerEmail;
}

QString Order::customerPhone() const {
    return m_customerPhone;
}

QList<SalesItem*> Order::orderItems() const {
    return m_orderItems;
}

double Order::totalPrice() const {
    double price = 0;
    for(SalesItem* item : m_orderItems) {
        price += item->price();
    }
    return price;
}

QString Order::orderSummary() const {
    QString summary;
    for(SalesItem* item : m_orderItems) {
        summary += item->name() + QString::fromStdString("(") + QString::number(item->amount()) +
                QString::fromStdString(") ");
    }
    return summary;
}

