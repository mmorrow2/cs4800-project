import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11


Window {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("El Nenes Cookies")

    property var unfulfilled_orders_view: "unfulfilled_orders";
    property var past_orders_view:        "past_orders";
    property var inventory_view:          "inventory";

    Loader {
        id: pageLoader
        width: 0
        height: 0
        anchors.left: column.right
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.leftMargin: 0
        sourceComponent: window_pane
        Component.onCompleted: loadView(unfulfilled_orders_view) // Initial View
    }

    function loadView(view) {
        pageLoader.source = view + ".qml"
    }
    
    ColumnLayout {
        id: column
        width: button.width
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        Button {
            id: button
            text: qsTr("Unfulfilled Orders")
            Layout.fillHeight: true
            Layout.fillWidth: true
            anchors.leftMargin: 0
            anchors.topMargin: 0

            onClicked: loadView(unfulfilled_orders_view)
        }

        Button {
            id: button2
            text: qsTr("Past Orders")
            Layout.fillHeight: true
            Layout.fillWidth: true
            anchors.leftMargin: 0
            anchors.topMargin: 0

            onClicked: loadView(past_orders_view)
        }

        Button {
            id: button1
            text: qsTr("Inventory")
            Layout.fillHeight: true
            Layout.fillWidth: true
            anchors.leftMargin: 0
            anchors.topMargin: 0

            onClicked: loadView(inventory_view)
        }

        Button {
            id: button3
            text: qsTr("Settings")
            Layout.fillHeight: true
            Layout.fillWidth: true
            anchors.leftMargin: 0
            anchors.topMargin: 0

            onClicked: loadView(past_orders_view)
        }
    }

    Component {
        id: window_pane
        Pane {
            width: 0
            height: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.left: column.right
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0
        }
    }

}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.75}D{i:4;anchors_height:400;anchors_x:0;anchors_y:20}
}
##^##*/
