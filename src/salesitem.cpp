#include "salesitem.hpp"

#include <string>

SalesItem::SalesItem(QString itemName, int amount, double itemPrice)
{
    m_name = itemName;
    m_amount = amount;
    m_price = itemPrice;
}

QString SalesItem::name() const {
    return m_name;
}

double SalesItem::price() const {
    return m_price;
}

int SalesItem::amount() const {
    return m_amount;
}
