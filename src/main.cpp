#include <QGuiApplication>
#include <QQmlApplicationEngine>
// include qml context, required to add a context property
#include <QQmlContext>
#include <QList>

#include <vector>
#include <iostream>
#include <fstream>

#include <order.hpp>
#include <salesitem.hpp>
#include <json.hpp>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

//TODO: Remove placeholder
#define COOKIE_PRICE 12


QString fromStr(const std::string str) {
    return QString::fromStdString(str);
}

int main(int argc, char *argv[])
{
    nlohmann::json j;

    std::ifstream i("../src/orders.json", std::ifstream::in);
    if(!i) {
        std::cout << "Error with file" << std::endl;
        return -1;
    }
    i>>j;

    curlpp::Cleanup myCleanup;
    curlpp::Easy easyhandle;
    easyhandle.setOpt(cURLpp::Options::Url("www.google.com"));
    easyhandle.perform();

    curlpp::terminate();

    QList<QObject*> orders;

    for(auto& element : j["orders"]) {
        std::cout << element << "\n";
        std::cout << typeid(element).name() << "\n";
        QList<SalesItem*> items;
        for(auto& order : element["order"].items()) {
            items.append(new SalesItem(fromStr(order.key()), (int)order.value(), (double)order.value() * COOKIE_PRICE));
        }
        Order* order = new Order(fromStr(element["name"]), fromStr(element["email"]), fromStr(element["phone"]), items);
        orders.append(order);
    }

    std::flush(std::cout);

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    engine.rootContext()->setContextProperty("orders", QVariant::fromValue(orders));

    engine.load(url);

    return app.exec();
}
