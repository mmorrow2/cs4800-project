
CS4800 Group Project
=======
# El Nenes Cookie Management System 

## Project Proposal

Our project proposal can be found 
[here](https://docs.google.com/document/d/10PDr3KvpaofCYsLXjhWYz_V8ta8YTABEO4li9hjwXKs/edit?usp=sharing)

## Details

Backend written in C++ and QML (QtQuick). Frontend JS website with Python/Flask REST API.
