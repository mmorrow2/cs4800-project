# Baker's Ear: API Documentation

## Setting Up MongoDB

[This guide](https://docs.mongodb.com/manual/administration/install-community/)
covers the installation of a MongoDB server for local use.

You will also need to have the following Python packages installed:

- [`pymongo`](https://pypi.org/project/pymongo/)

---

## Environment Variables

### Using MongoDB cloud service.

The API requires read and write access to the cloud database.
In order for this to occur securely, the system being used to run the server
must have the proper environment variables set up.

These variables are:

- `CS4800_DB_USER`: Database username.
- `CS4800_DB_PASS`: Database password.

Attempting to connect to the cloud service, without the proper environment variables,
will yield errors to the console.

### Using local MongoDB database.

The environment variables do not need to be set when working with a local database.
This allows for local testing without sharing usernames and passwords.

``` python
""" Set up a local database connection."""
from db_utils import DBTools

# Initialize a database connection.
db = DBTools()
# Instruct DBTools instance to use local database.
db_tools.local = True  # This is default behavior.
```
