import os
from datetime import datetime
from pymongo import MongoClient
from bson.objectid import ObjectId


class BaseDBTools(object):
    """ Handles connecting to a local or remote database.

    Attributes
    ----------
        local : Boolean
            Controls the use of a local/cloud database.
            Set to 'False' to connect to cloud database. Defaults to local database.

    """

    def __init__(self):
        self.local = True
        self.client = MongoClient()

    @property
    def local(self):
        """Return object's `local` variable."""
        return self.local

    @local.setter
    def local(self, local):
        """ Controls the connection to database on MongoDB cloud.

        Assumes that username and password are stored as environment variables
        CS4800_DB_USER and CS4800_DB_PASS, respectively.
        This avoids the exposure of secret keys via push to GitHub.
        """
        if not local:
            # Get DB username from environment var.
            db_user = os.environ.get('CS4800_DB_USER')
            # Get DB password from environment var.
            db_pass = os.environ.get('CS4800_DB_PASS')

            assert len(db_user) > 0, "DB-Username not defined in environment."
            assert len(db_pass) > 0, "DB-Password not defined in environment."

            # Create connection string using user/pass.
            conn_str = F'mongodb+srv://{db_user}:{db_pass}@[...]'
            self.client = MongoClient(
                conn_str, connectTimeoutMS=5000, connect=True)


class DBTools(BaseDBTools):
    """ This module provides CRUD operators for a database. 

    Note
    ----
        To connect to live database, set instance's 'local' attribute to False.
        `(UserDBTools Instance).local = False`

    Functions
    ---------
        create: Create a new order.

        read: Read in all orders.

        update: Update an existing order.

        delete: Delete an existing order.
    """

    def __init__(self):
        super().__init__()

    def create(self, order_form):
        """ Add new order to 'orders' collection in database.

        Parameters
        ----------
            order_form: Dictionary
                Dictionary containing order information.

        Returns
        -------
            True: Order created successfully.

            False: Order not created successfully.
        """

        self.client.db.orders.insert_one({"date": datetime.utcnow()})

        return True

    def read(self):
        """ Read all orders from database.

        Returns
        -------
            orders: List of orders.
        """

        orders = []  # Create container for all orders.
        # Get all orders. Do not include ObjectIDs of orders.
        for order in list(self.client.db.orders.find({}, {"_id": 0})):
            orders.append(order)

        return orders

    def update(self, id):
        """ Update details of an existing order.

        Parameters
        ----------
            id: ObjectID of order to update.

        Returns
        -------
            True: Update was successful.
            False: Update was not successful.
        """

        updated_order = {}
        self.client.db.orders.update_one({"_id": id},
                                         {"$set": updated_order})
        return True

    def delete(self, id):
        """ Delete an existing order.

        Parameters
        ----------
            id: ObjectID of order to delete.

        Returns
        -------
            True: Deletion was successful.
            False: Deletion was not successful.
        """

        result = self.client.db.orders.delete_one({"_id": id})
        if result.deleted_count == 0:
            return False

        return True
