"""
# Main Application File

## Flask

A Flask server is initialized in this app.
This server will handle API requests for interfacing with a database.
"""

from flask import Flask, jsonify, request
from db_utils import DBTools

# instantiate database connection.
db = DBTools()
db.local = True
# instantiate the app
app = Flask(__name__)

# Start API routing.
@app.route("/api/ping", methods=["GET"])
def ping_pong():
    """ A must-have sanity check."""
    return jsonify('Pong!')

@app.route("/api/submit_order", methods=["POST"])
def submit_order():
    """ Submit order to database."""

    response = {"success": False,}  # Create response dict.
    if request.method == "POST":  # Check for POST before collecting JSON.
        order = request.get_json()  # Collect JSON input.
        if db.create(order):  # Submit order, wait for status.
            response["success"] = True  # Order submitted successfully.

    return response

@app.route('/api/read_all', methods=["GET"])
def read_all():
    """ Read all orders in database."""

    orders = db.read()
    return jsonify(orders)


if __name__ == '__main__':
    app.run()
